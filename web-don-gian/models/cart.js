module.exports = function Cart(oldCart) {
	this.items = oldCart.items || {};
	this.totalQty = oldCart.totalQty || 0;
	this.totalPrice = oldCart.totalPrice || 0;

	this.add = function(item, id) {
		var storedItem = this.items[id];
		if (!storedItem) {
			storedItem = this.items[id] = {item: item, qty: 0, price: 0, totalItem:0};
		}
		storedItem.qty++;
		storedItem.price = storedItem.item.price;
		storedItem.totalItem = storedItem.item.price * storedItem.qty;
		this.totalQty++;
		this.totalPrice += storedItem.item.price;
	};

	this.reduce = function(item, id) {
		var storedItem = this.items[id];
		storedItem.qty--;
		this.totalQty--;
		storedItem.totalItem = storedItem.item.price * storedItem.qty;
		storedItem.price = storedItem.item.price;
		this.totalPrice -= storedItem.price;
		if(storedItem.qty < 1) {
            delete this.items[id];
        }
	};
	
	this.remove = function (item, id) {
		var storedItem = this.items[id];
		storedItem.totalItem = storedItem.item.price * storedItem.qty;
		this.totalQty -= storedItem.qty;
        this.totalPrice -= storedItem.totalItem;
        delete this.items[id];
    };
	this.removeHome = function (id) {
		this.totalQty -= this.items[id].qty;
		// xoá toàn bộ giá sp
        this.totalPrice -= this.items[id].item.price * this.items[id].qty;
        delete this.items[id];
    };


	this.generateArray = function() {
		var arr = [];
		for( var id in this.items) {
			arr.push(this.items[id]);
		}
		return arr;
	};
};