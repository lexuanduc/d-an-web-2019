const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var productSchema = new Schema({
	name: {type: String, require: true},
	title: {type: String, require: true},
	slug: {type: String},
	category: {type: String, require: true},
	images: {type: String, require: true},
	price: {type: Number, require: true},
	salePrice: {type: Number},
	date: { type: String},
	count: {type: Number}
});

module.exports = mongoose.model('Product', productSchema, 'products');