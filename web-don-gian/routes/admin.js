var express = require('express');
var router = express.Router();

var mkdirp = require('mkdirp');
var fs = require('fs-extra');
var resizeImg = require('resize-img');
var paginate  = require('express-paginate');

var User = require('../models/user');
var Product = require('../models/product');
var Category = require('../models/category');

router.get('/page', function(req, res, next) {
	res.render('admin/page', {
		title: 'page-admin'
	});
});

router.get('/all-product', function(req, res) {
	Category.find(function(err, categorys) {
		var categoryChunks = [];
		categoryChunks.push(categorys);

		Product.find(function(err, products) {
			var productChunks = [];
			productChunks.push(products);

			var dem = 0;
			for(var j = 1; j <= products.length; j++) {
				dem++;
			}
			res.render('admin/all-product', {
				title: 'All Product Admin',
				products: productChunks,
				categorys: categoryChunks,
				dem: dem
			});
		});	
	});	
});

router.get('/all-product/:category', function(req, res) {
	var categorySlug = req.params.category;

	Category.find(function(err, categorys) {
		var categoryChunks = [];
		categoryChunks.push(categorys);

		Category.findOne({"slug": categorySlug}, function(err, c) {
			Product.find({"category": categorySlug}, function(err, products) {
				if (err) console.log(err);
				var productChunks = [];
				productChunks.push(products);

				res.render('admin/all-product', {
					title: c.title,
					products: productChunks,
					categorys: categoryChunks,
					categorySlug: categorySlug
				});
			});
		});
	});	
});

router.get('/product', function(req, res) {
	var successMsg = req.flash('success')[0];
	Product.find(function(err, products) {
		var productChunks = [];
		productChunks.push(products);

		var dem = 0;
		for(var j = 1; j <= products.length; j++) {
			dem++;
		}
		res.render('admin/product', {
			title: 'Add Product Admin',
			products: productChunks,
			dem: dem,
			successMsg: successMsg
		});
	});	
	
});
router.get('/user', function(req, res, next) {
	var successMsg = req.flash('success')[0];
	User.find(function(err, user) {
		var userChunks = [];
		userChunks.push(user);

		var count;
		for(var j = 1; j <= user.length; j++) {
			count++;
		}
		res.render('admin/user', {
			title: "Admin User",
			user: userChunks,
			successMsg: successMsg
		});
	});
	
});

/*
						delete user
*/
router.get('/user/delete-user/:id', function(req, res) {
	var id = req.params.id;
		
	User.findByIdAndRemove(id, function(err) {
		console.log(err);
	});

	req.flash('success', 'User Deleted!');
	res.redirect('/admin/user');
});


router.get('/category', function(req, res, next) {
	var successMsg = req.flash('success')[0];
	Category.find(function(err, categorys) {
		var categoryChunks = [];
		categoryChunks.push(categorys);
		res.render('admin/category', {
			title: "category",
			categorys: categoryChunks,
			successMsg: successMsg
		});
	});
	
});

/*add category*/
router.get('/category/add-category', function(req, res) {
	var title = "";

	res.render('admin/addCategory', {
		title: title
	});
});


router.post('/category/add-category', function(req, res) {

	req.checkBody('title', 'Title must have a value').notEmpty();

	var title = req.body.title;
	var slug = title.replace(/\s+/g, '-').toLowerCase();

	var errors = req.validationErrors();
	if (errors) {
		var messages = [];
		errors.forEach(function(error) {
			messages.push(error.msg);
		});
		res.render('admin/addCategory', {
			errors: messages,
			title: title
		});
	} else {
		Category.findOne({"slug": slug}, function(err, category) {
			if (category) {
				req.flash('danger', 'Category title exists, choose another.');
				res.render('admin/addCategory', {
					title: title
				});
			} else {
				
				var category = new Category({
						title: title,
						slug: slug
					});
				category.save(function(err) {
					if (err) return console.log(err);
					Category.find(function(err, categories) {
						if (err) {
						    console.log(err);
						} else {
						    req.app.locals.categories = categories;
						}
					});
					req.flash('success', 'Category added!');
					res.redirect('/admin/category');
				});
			}
		});
	}
});



/*
							edit category
*/
router.get('/category/edit-category/:id', function(req, res) {

	Category.findById(req.params.id, function(err, c) {
		if (err)	console.log(err);
		res.render('admin/editCategory', {
			title: c.title,
			id: c._id
		});
	});
});

router.post('/category/edit-category/:id', function(req, res) {

	req.checkBody('title', 'Title must have a value').notEmpty();

	var id = req.params.id;
	var title = req.body.title;
	var slug = title.replace(/\s+/g, '-').toLowerCase();

	var errors = req.validationErrors();
	if (errors) {
		var messages = [];
		errors.forEach(function(error) {
			messages.push(error.msg);
		});
		res.render('admin/editCategory', {
			errors: messages,
			title: title,
			id: id
		});
	} else {
		Category.findOne({"slug": slug,_id: {'$ne' : id}}, function(err, category) {
			if (category) {
				req.flash('danger', 'Category title exists, choose another.');
				res.render('admin/editCategory', {
					title: title,
					id: id
				});
			} else {	
				Category.findById(id, function(err, c) {
					if (err) console.log(err);
					c.title = title;
					c.slug = slug;
					
					c.save(function(err) {
						if (err) console.log(err);

						Category.find(function(err, categories) {
							if (err) {
							    console.log(err);
							} else {
							    req.app.locals.categories = categories;
							}
						});

						req.flash('success', 'Category edited!');
						res.redirect('/admin/category');
					});
				});
			}
		});
	}
});


/*
						delete category
*/
router.get('/category/delete-category/:id', function(req, res) {
	var id = req.params.id;
	
	Category.findByIdAndRemove(id, function(err) {
		if (err) 
		return console.log(err);
	});
	Category.find(function(err, categories) {
		if (err) {
		    console.log(err);
		} else {
		    req.app.locals.categories = categories;
		}
	});
	req.flash('success', 'Category Deleted!');
	res.redirect('/admin/category');
});




/*add product*/
router.get('/product/add-product', function(req, res) {
	var name = "";
	var title = "";
	var price = "";
	var salePrice = "";

	Category.find(function(err, categories) {
		var categoryChunks = [];
		categoryChunks.push(categories);
		res.render('admin/addProduct', {
			name: name,
			title: title,
			categories: categoryChunks,
			price: price,
			salePrice: salePrice
		});
	});
});

router.post('/product/add-product', function(req, res) {
	var imageFile = typeof req.files.images !== 'undefined' ? req.files.images.name: "";
	var date = new Date();

	var y = date.getFullYear();
	var m = date.getMonth() + 1;
	var d = date.getDate();
	var h = date.getHours();
	var mn = date.getMinutes();
	var s = date.getSeconds();

	var date1 = y + "/" + m + "/" + d + " " + h + ":" + mn + ":" + s;

	req.checkBody('name', 'Name must have a value').notEmpty();
	req.checkBody('title', 'Title must have a value').notEmpty();
	req.checkBody('price', 'price must have a value').isDecimal();
	req.checkBody('images', 'Images must have a value').isImage(imageFile);

	
	var title = req.body.title;
	var slug = title.replace(/\s+/g, '-').toLowerCase();
	var category = req.body.category;
	var name = req.body.name;
	var price = req.body.price;
	var salePrice = req.body.salePrice;
	var images = req.body.images;

	var count = 0;
	Product.find(function(err, item) {
		for(var j = 1; j <= item.length; j++) {
			count++;
		}
	});

	var errors = req.validationErrors();
	if (errors) {
		var messages = [];
		errors.forEach(function(error) {
			messages.push(error.msg);
		});

		Category.find(function(err, categories) {
			var categoryChunks = [];
			categoryChunks.push(categories);
			res.render('admin/addProduct', {
				errors: messages,
				name: name,
				title: title,
				categories: categoryChunks,
				price: price,
				salePrice: salePrice
			});
		});
	} else {
		Product.findOne({"slug": slug}, function(err, product) {
			if (product) {
				req.flash('danger', 'Product title exists, choose another.');

				Category.find(function(err, categories) {
					var categoryChunks = [];
					categoryChunks.push(categories);
					res.render('admin/addProduct', {
						errors: messages,
						name: name,
						title: title,
						categories: categoryChunks,
						price: price,
						salePrice: salePrice
					});
				});
			} else {
				var price2 = parseFloat(price).toFixed(2);
				var salePrice2 = parseFloat(salePrice).toFixed(2);
				count++;
				var product = new Product({
						name: name,
						title: title,
						slug: slug,
						category: category,
						price: price2,
						salePrice: salePrice2,
						images: imageFile,
						count: count,
						date: date1
					});
				product.save(function(err) {
					if (err) return console.log(err);
					
					mkdirp('public/images/product-img/'+ product._id, function(err) {
						return console.log(err);
					});	
			
					if (imageFile != "") {
						var productImage = req.files.images;
						var path = 'public/images/product-img/'+ product._id + '/' + imageFile;

						productImage.mv(path, function(err) {
							return console.log(err);
						});
					}
					req.flash('success', 'Product added!');
					res.redirect('/admin/product');
				});
			}
		});
	}
});


/*
							edit product
*/
router.get('/product/edit-product/:id', function(req, res) {
	var errors;

	if (req.session.errors) {
		errors = req.session.errors;
	}
	req.session.errors = null;

	Category.find(function(err, categories) {
		var categoryChunks = [];
		categoryChunks.push(categories);
		Product.findById(req.params.id, function(err, p) {
			if (err) {
				console.log(err);
				res.redirect('/admin/product');
			} else {
				var galleryDir = 'public/images/product-img/'+ p._id + '/gallery';
				var galleryImages = null;

				fs.readdir(galleryDir, function(err, files) {
					if (err) console.log(err);
					galleryImages = files;
					res.render('admin/editProduct', {
						errors: errors,
						name: p.name,
						title: p.title,
						categories: categoryChunks,
						category: p.category,
						price: parseFloat(p.price).toFixed(2),
						salePrice: parseFloat(p.salePrice).toFixed(2),
						images: p.images,
						galleryImages: galleryImages,
						id: p._id
					});
				});
			}
		});
	});
});

router.post('/product/edit-product/:id', function(req, res) {
	var imageFile = typeof req.files.images !== 'undefined' ? req.files.images.name: "";

	req.checkBody('name', 'Name must have a value').notEmpty();
	req.checkBody('title', 'Title must have a value').notEmpty();
	req.checkBody('price', 'price must have a value').isDecimal();
	req.checkBody('images', 'Images must have a value').isImage(imageFile);

	var name = req.body.name;
	var title = req.body.title;
	var slug = title.replace(/\s+/g, '-').toLowerCase();
	var category = req.body.category;
	var price = req.body.price;
	var salePrice = req.body.salePrice;
	var pimage = req.body.pimage;
	var id = req.params.id;

	var errors = req.validationErrors();
	if (errors) {
		req.session.errors = errors;
		res.redirect('/admin/product/edit-product/'+ id);
	} else {
		Product.findOne({"title": title, _id: {'$ne' : id}}, function(err, p) {
			if (err) console.log(err);

			if (p) {
				req.flash('danger', 'Product title exists, choose another.');
				res.redirect('/admin/product/edit-product/'+id);
			} else {
				Product.findById(id, function(err, p) {
					if (err) console.log(err);
					p.name = name;
					p.title = title;
					p.slug = slug;
					p.category = category;
					p.price = parseFloat(price).toFixed(2);
					p.salePrice = parseFloat(salePrice).toFixed(2);
					if (imageFile != "") {
						p.images = imageFile;
					}
					p.save(function(err) {
						if (err) console.log(err);
						if (imageFile != "") {
							if (pimage != "") {
								fs.remove('public/images/product-img/'+ id + '/' + pimage, function(err) {
									if (err) console.log(err);
								});
							}

							var productImage = req.files.images;
							path = 'public/images/product-img/'+ id + '/' + imageFile;

							productImage.mv(path, function(err) {
								return console.log(err);
							});
						}
						req.flash('success', 'Product edited!');
						res.redirect('/admin/product');
					});
				});
			}
		});
	}

});


/*
						delete product
*/
router.get('/product/delete-product/:id', function(req, res) {
	var id = req.params.id;
	
	var path = 'public/images/product-img/'+ id;
	fs.remove(path, function(err) {
		if (err) console.log(err);	
			
		Product.findByIdAndRemove(id, function(err) {
			console.log(err);
		});

		req.flash('success', 'Product Deleted!');
		res.redirect('/admin/product');
	});
});






module.exports = router;