const express = require('express');
const router = express.Router();
var Product = require('../models/product');
var Category = require('../models/category');


router.get('/toshiba', function(req,res) {

	Category.find(function(err, categorys) {
		var categoryChunks = [];
		categoryChunks.push(categorys);

		Category.findOne({"slug": "television"}, function(err, c) {
			Product.find({"category": "television"}, function(err, products) {
				if (err) console.log(err);
				var toshibaProducts = [];
				toshibaProducts.push(products);

				var latestProducts1 = [];
				// productContent 3.....16 -> 22
					Product.find().skip(0).limit(4).exec(function(err, latest) {
						latestProducts1.push(latest);
					});
				var latestProducts2 = [];
				// productContent 3.....16 -> 22
					Product.find().skip(5).limit(4).exec(function(err, latest) {
						latestProducts2.push(latest);
					});
				

				var recentlyViewed = [];
				// productContent 3.....16 -> 22
					Product.find().skip(0).limit(10).exec(function(err, docs) {
						recentlyViewed.push(docs);
					});
					

				res.render('catalog/toshiba', {
					title: 'Toshiba Shop',
					latestProducts1: latestProducts1,
					latestProducts2: latestProducts2,
					toshibaProducts: toshibaProducts,
				});	recentlyViewed: recentlyViewed
			});
		});
	});	
});

router.get('/toshiba', function(req,res) {
	var toshibaProducts = [];
	Product.find().skip(0).limit(12).exec(function(err, docs) {
		toshibaProducts.push(docs);

		var latestProducts1 = [];
		// productContent 3.....16 -> 22
			Product.find().skip(0).limit(4).exec(function(err, latest) {
				latestProducts1.push(latest);
			});
		var latestProducts2 = [];
		// productContent 3.....16 -> 22
			Product.find().skip(5).limit(4).exec(function(err, latest) {
				latestProducts2.push(latest);
			});
		

		var recentlyViewed = [];
		// productContent 3.....16 -> 22
			Product.find().skip(0).limit(10).exec(function(err, docs) {
				recentlyViewed.push(docs);
			});
			

		res.render('catalog/toshiba', {
		title: 'Toshiba Shop',
		latestProducts1: latestProducts1,
		latestProducts2: latestProducts2,
		toshibaProducts: toshibaProducts,
		recentlyViewed: recentlyViewed
		});
	});	
});




module.exports = router;