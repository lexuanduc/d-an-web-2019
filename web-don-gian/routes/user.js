var express = require('express');
var router = express.Router();
var passport = require('passport');

var csrf = require('csurf');
var csrfProtection = csrf();
router.use(csrfProtection);

var Cart = require('../models/cart');
var Order = require('../models/order');

router.get('/profile', isLoggedIn, function(req, res, next) {
	var successMsg = req.flash('success')[0];
	var user = res.locals.user;
	var adminLogin = false;
	if (user.email == "admin@gmail.com") {
		adminLogin = true;
	}
	Order.find({"user": req.user}, function(err, orders) {
        if(err) {
            return res.write('Error!');
        }
        var cart;
        orders.forEach(function (order) {
            cart = new Cart(order.cart);
            order.items = cart.generateArray();
        });
	    res.render('user/profile', {
			user: user,
			admin: adminLogin,
			successMsg: successMsg,
			orders: orders
		});
    });
	
});

router.get('/logout', isLoggedIn, function(req, res, next) {
	req.logout();
	res.locals.user = null;
	res.redirect('/');
});


// dang ki
router.get('/signup', function(req, res, next) {
	var messages = req.flash('error');
	res.render('user/signup', {csrfToken: req.csrfToken(), messages: messages, hasErrors: messages.length > 0});
});


router.post('/signup', passport.authenticate('local.signup', {
	successRedirect: '/user/signin',
	failureRedirect: '/user/signup',
	failureFlash: true
}));

// dang nhap
router.get('/signin', function(req, res, next) {
	var messages = req.flash('error');
	res.render('user/signin', {csrfToken: req.csrfToken(), messages: messages, hasErrors: messages.length > 0});
});

router.post('/signin', passport.authenticate('local.signin', {
	failureRedirect: '/user/signin',
	failureFlash: true
}), function (req, res, next) {
    if(req.session.oldUrl) {
        var oldUrl = req.session.oldUrl;
        req.session.oldUrl = null;
        res.redirect('/cart'+oldUrl);
    } else {
        res.redirect('/user/profile');
    }
});

module.exports = router;

// da dang nhap
function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}
	// chua co dang nhap
	res.redirect('/user/signin');
}

// chua dang nhap
function notLoggedIn(req, res, next) {
	if (!req.isAuthenticated()) {
		return next();
	}
	//co dang nhap roi
	res.redirect('/');
}