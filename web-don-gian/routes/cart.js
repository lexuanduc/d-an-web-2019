var express = require('express');
var router = express.Router();

var fs = require('fs-extra');
var Cart = require('../models/cart');
var Order = require('../models/order');
var Product = require('../models/product');


// them gio hang
router.get('/add-to-cart/:id', function(req, res) {
	var productId = req.params.id;
	var cart = new Cart(req.session.cart ? req.session.cart : {});

	Product.findById(productId, function(err, product) {
		if (err) {
			return res.redirect('/');
		}
		cart.add(product, product.id);
		req.session.cart = cart;
		res.redirect('/');
	});
});


// check gio hang
router.get('/shopping-cart', function(req, res, next) {
	if (!req.session.cart) {
		return res.render('shop/shopping-cart', {productsItems: null});
	}
	var cart = new Cart(req.session.cart);
	
	var RecentlyViewed = [];
	// productContent 3.....16 -> 22
		Product.find().skip(0).limit(10).exec(function(err, docs) {
			RecentlyViewed.push(docs);
		});
	res.render('shop/shopping-cart', {
			title: "Shopping Cart",
			productsItems: cart.generateArray(), 
			totalPrice: cart.totalPrice,
			RecentlyViewed: RecentlyViewed
		});
});


// show gio hang
router.get('/show-cart/:id/:img', function(req, res, next) {
	var id = req.params.id;
	var images = req.params.img;
	var name = "";
	var title = "";
	Product.findById(id, function(err, p) {
		res.render('shop/show-cart', {
			id: id,
			images: images,
			sp: p
		});
	});
	
});


// them sua xoa
router.get('/update/:id', function(req, res) {
	var productId = req.params.id;
	var action = req.query.action;
	var cart = new Cart(req.session.cart ? req.session.cart : {});

	Product.findById(productId, function(err, product) {
		if (err) {
			return res.redirect('/');
		}
		switch(action) {
			case "add":
				cart.add(product, product.id);
				req.session.cart = cart;
				break;
			case "reduce":
				cart.reduce(product, product.id);
				req.session.cart = cart;
				break;
			case "remove":
				cart.remove(product, product.id);
				req.session.cart = cart;
				break;
		}
		res.redirect('/cart/shopping-cart');
	});
});


// xóa sản phẩm trong trang home
router.get('/remove/:id', function (req, res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});
  
    cart.removeHome(productId);
    req.session.cart = cart;
    res.redirect('/');
});


// check mua hang
/*router.get('/checkout', function(req, res, next) {
	if (req.session.cart && req.session.cart.length == 0) {
		delete req.session.cart;
		res.redirect('/cart/checkout');
	}

	var errMsg = req.flash('error')[0];
	res.render('shop/checkout', {
		title: "checkout",
		cart: req.session.cart, 
		errMsg: errMsg, 
		noError: !errMsg
	});
});
*/
router.get('/checkout', isLoggedIn, function (req, res, next) {
    if(!req.session.cart) {
        return res.redirect('/shopping-cart');
    }
    var cart = new Cart(req.session.cart);
    var errMsg = req.flash('error')[0];
    return res.render('shop/checkout', {total: cart.totalPrice, errMsg: errMsg, noError: !errMsg});
});


// mua hang xong reset
router.post('/checkout', isLoggedIn, function(req, res, next) {
    if(!req.session.cart) {
        return res.redirect('/shopping-cart');
    }
    var date = new Date();
    var y = date.getFullYear();
	var m = date.getMonth() + 1;
	var d = date.getDate();
	var h = date.getHours();
	var mn = date.getMinutes();
	var s = date.getSeconds();
	var date1 = y + "/" + m + "/" + d + " " + h + ":" + mn + ":" + s;

    var cart = new Cart(req.session.cart);
    var order = new Order({
        user: req.user,
        cart: cart,
        date: date1,
        address: req.body.address,
        name: req.body.name,
        phone: req.body.phone
    });
    order.save(function(err, result) {
        req.flash('success', 'Successfully bought product!');
        req.session.cart = null;
        res.redirect('/user/profile');
    });
});

module.exports = router;

function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    req.session.oldUrl = req.url;
    res.redirect('/user/signin');
}