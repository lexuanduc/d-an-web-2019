document.addEventListener('DOMContentLoaded', function() {
	var sortActive = document.querySelectorAll('.sort-icons i');
	var showActive = document.querySelectorAll('.accessories-show');

	
	for(s of sortActive) {
		s.addEventListener('click', function() {
			var currentSortActive = document.querySelector('.sort-icons i.sort-active');
			var currentShowActive = document.querySelector('.accessories-show.accessories-active');

			currentSortActive.classList.remove('sort-active');
			currentShowActive.classList.remove('accessories-active');

			this.classList.add('sort-active');
			var arrSort = Object.values(sortActive);
			showActive[arrSort.indexOf(this)].classList.add('accessories-active');
		});
	}


},false);

$(function() {
	$('.catalog-list li.open').click(function() {
		$(this).toggleClass('toshiba-active');
	});
	
	$('.list-filter li.open').click(function() {
		$(this).toggleClass('filter-active');
	});
})