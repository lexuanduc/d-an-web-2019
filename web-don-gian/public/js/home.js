document.addEventListener('DOMContentLoaded', function() {
	var menuLeft = document.querySelector('.menu-left');
	var mainNav = document.querySelector('.main-nav');

	var prew = document.querySelector('.prew');
	var prewCart = document.querySelector('.prew-cart');
	var html = document.querySelector('html');
	var closeItem = document.querySelector('.close-item');
	var black = document.querySelector('.black');

	//view products
	var views = document.querySelectorAll('.fa.fa-eye');
	var productButton = document.querySelectorAll('.product-button');

	var dots = document.querySelectorAll('.buttom ul li');
	var videos = document.querySelectorAll('.video');

	var video1 = document.getElementById('video1');
	var video2 = document.getElementById('video2');
	var video3 = document.getElementById('video3');

	// display menu
	menuLeft.addEventListener('click', function() {
		menuLeft.classList.toggle('menu-active');
		mainNav.classList.toggle('main-active');
	});

	// code prewCart
	prew.addEventListener('click', function() {
		prewCart.classList.add('prew-active');
		html.style.overflowY = "hidden";
	});
	closeItem.addEventListener('click', function() {
		prewCart.classList.remove('prew-active');
		console.log(prewCart);
		html.style.overflowY = "visible";
	});

	black.addEventListener('click', function() {
		prewCart.classList.remove('prew-active');
		console.log(prewCart);
		html.style.overflowY = "visible";
	});

	// code buttom-circle
	for(x of dots) {
		x.addEventListener('click', function() {
			let currentDot1 = document.querySelector('.buttom ul li.active');
			let currentSlide1 = document.querySelector('.video.active');

			currentDot1.classList.remove('active');
			currentSlide1.classList.remove('active');

			this.classList.add('active');
			let arrDots = Object.values(dots);
			videos[arrDots.indexOf(this)].classList.add('active');
			videos[arrDots.indexOf(this)].load();
		});
	} // End for

	// nextSlide
	function toNextSlide() {
		let currentDot2 = document.querySelector('li.active');
		let currentSlide2 = document.querySelector('.video.active');

		console.log(currentDot2);
		console.log(currentSlide2);

		currentDot2.classList.remove('active');
		currentSlide2.classList.remove('active');

		if (currentSlide2.nextElementSibling != null) {
			currentSlide2.nextElementSibling.classList.add('active');
			currentDot2.nextElementSibling.classList.add('active');
		} else if (currentSlide2.nextElementSibling == (length -1)) {
			videos[0].classList.add('active');
			dots[0].classList.add('active');
		}
	} // end toNextSlide

	video1.onended = function() {
		video2.play();
		toNextSlide();
	};
	video2.onended = function() {
		video3.play();
		toNextSlide();
	};
	video3.onended = function() {
		video1.play();
	};

	// code products content
	var navTabs = document.querySelectorAll('.nav-tabs li');
	var content = document.querySelectorAll('.content');
	for(l of navTabs) {
		l.addEventListener('click', function() {
			var currentnavTabs = document.querySelector('.nav-tabs li.nav-active');
			var currentContent = document.querySelector('.content.content-active');

			currentnavTabs.classList.remove('nav-active');
			currentContent.classList.remove('content-active');

			this.classList.add('nav-active');
			var arrList = Object.values(navTabs);
			content[arrList.indexOf(this)].classList.add('content-active');
		});
	} // End for

	// code products main
	var listMenu = document.querySelectorAll('.product-list-menu li');
	var contentWrapTab = document.querySelectorAll('.wrap-tab');
	for(m of listMenu) {
		m.addEventListener('click', function() {
			var currentTabsActive = document.querySelector('.product-list-menu li.tabs-active');
			var currentMainActive = document.querySelector('.wrap-tab.main-active');

			currentTabsActive.classList.remove('tabs-active');
			currentMainActive.classList.remove('main-active');

			this.classList.add('tabs-active');
			var arrList = Object.values(listMenu);
			contentWrapTab[arrList.indexOf(this)].classList.add('main-active');
		});
	} // End for

	// code tabs-panel-content
	var listPanel = document.querySelectorAll('.list-panel li');
	var tabsPanel = document.querySelectorAll('.tabs-panel');
	for(p of listPanel) {
		p.addEventListener('click', function() {
			let currentListPanelActive = document.querySelector('.list-panel li.panel-active');
			let currentTabsPanelActive = document.querySelector('.tabs-panel.tabs-panel-active');

			currentListPanelActive.classList.remove('panel-active');
			currentTabsPanelActive.classList.remove('tabs-panel-active');

			this.classList.add('panel-active');
			let arrPanel = Object.values(listPanel);
			tabsPanel[arrPanel.indexOf(this)].classList.add('tabs-panel-active');
		});
	}

	// nút trượt lên trên top
	window.addEventListener('scroll',function(){
		if(window.pageYOffset > 700){
			$('.buttonIcon').addClass('go-up');
		}else {
			$('.buttonIcon').removeClass('go-up');
		}
	});
	var nut = document.querySelector('.buttonIcon');
	 nut.onclick = function(){
	 	var chieucaoht = self.pageYOffset;
	 	var set = chieucaoht;
	 	var run = setInterval(function(){
			chieucaoht = chieucaoht - 0.05*set;
            window.scrollTo(0,chieucaoht);
			if(chieucaoht <= 0) {
	            clearInterval(run);
	        }        
        },30);
	};
	
},false);

	
$(function() {
	
	

	// show-cart-size wrap-size
	var showCartSizes = $('.wrap-size span');
	showCartSizes.each(function () {
		$(this).click(function() {
			showCartSizes.removeClass('size-active');
			$(this).addClass('size-active');
		}); 
	});

	// dots i
	var dotsClassI = $('.dots i');
	dotsClassI.each(function () {
		$(this).click(function() {
			dotsClassI.removeClass('dot-active');
			$(this).addClass('dot-active');
		}); 
	});



	
	var showCartImgs = $('.draggable img');
	var CartImg = $('.cart-img');
	
	showCartImgs.each(function () {
		var imgChange = $(this).attr('src');	
		$(this).click(function() {
			showCartImgs.removeClass('img-active');
			$(this).addClass('img-active');
			CartImg.attr("src", imgChange);
		});
	});

	// kiem tra xem co muốn xoa ko
	$('a.confirmDeletion').on('click', function() {
		if (!confirm('Confirm deletion')) {
			return false;
		}
	});

	//owl-Carousel
    $('.owl-one.owl-carousel').owlCarousel({
        loop:false, // ko lặp lại vô hạn
        nav:true, // có thanh menu
        responsive:{
            0:{
                items:1 // khi mh 0 thì hthi 1 ảnh
            },
            600:{
                items:3 // khi mh 600 thì hthi 3 ảnh
            },
            1000:{
                items:3 // khi mh 1000 thì hthi 3 ảnh
            }
        }
    });
    $('.owl-two.owl-carousel').owlCarousel({
        loop:true, // lặp lại vô hạn
        nav:true,
        autoplay:true,
        autoplayTimeout:2000, // thời gian chạy 2 giây
        autoplayHoverPause:true, // trỏ chuột và thì dừng lại
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:6
            }
        }
    });
    $('.owl-three.owl-carousel').owlCarousel({
        loop:false,
        nav:true,
        dots:false, // ko cho hiển thị chấm tròn
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    });
	$('.modal-owl.owl-carousel').owlCarousel({
        loop:false,
        nav:true,
        dots:false,
        margin:30,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1200:{
                items:1
            }
        }
    });
    $('.show-cart-owl.owl-carousel').owlCarousel({
        loop:false,
        nav:true,
        dots:false,
        margin:30,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1200:{
                items:1
            }
        }
    });
    //owl-Carousel
    $('.owl-latest.owl-carousel').owlCarousel({
        loop:false, // ko lặp lại vô hạn
        nav:true, // có thanh menu
        dots:false,
        responsive:{
            0:{
                items:1 // khi mh 0 thì hthi 1 ảnh
            },
            600:{
                items:1 // khi mh 600 thì hthi 3 ảnh
            },
            1000:{
                items:1 // khi mh 1000 thì hthi 3 ảnh
            }
        }
    });
		
});
