module.exports.adminLogin = function(req, res, next) {
	var admin = res.locals.user;
	var emailAdmin = "admin@gmail.com";
	if (!admin) {
		res.redirect('/user/signin');
		return;
	}
	if (admin.email !== emailAdmin) {
		res.redirect('/');
		return;
	}
	next();
};