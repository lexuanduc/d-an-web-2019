var Product = require('../models/product');
var User = require('../models/user');
var Cart = require('../models/cart');

module.exports.indexProduct = function(req, res, next) {
	var successMsg = req.flash('success')[0];
	var cart = new Cart(req.session.cart ? req.session.cart : {});

	Product.find(function(err, product) {
		var productsItems = [];
		productsItems.push(product);

		var user = res.locals.user;

		var productChunks = [];
		var productChunks1 = [];
		var productChunks2 = [];

		//produc sale 1
		Product.find({"name": "sale"}, function(err, item) {
			productChunks.push(item);
		});
		//produc sale 2
		Product.find({"name": "sale1"}, function(err, item) {
			productChunks1.push(item);
		});
		//produc sale 3
		Product.find({"name": "sale2"}, function(err, item) {
			productChunks2.push(item);
		});
	
		var productContent = [];
		var productContent1 = [];
		var productContent2 = [];
		var productContent3 = [];
		// productContent 0.... từ vị trí 22 lấy 3 phần tử
			Product.find().skip(20).limit(3).exec(function(err, docs) {
				productContent.push(docs);
			});
		// productContent 1.... từ vị trí 2 lấy 6 phần tử
			Product.find().skip(0).limit(6).exec(function(err, docs) {
				productContent1.push(docs);
			});
		// productContent 2.....từ vị trí 9 lấy 6 phần tử
			Product.find().skip(8).limit(6).exec(function(err, docs) {
				productContent2.push(docs);
			});
		// productContent 3.....từ vị trí 16 lấy 6 phần tử
			Product.find().skip(14).limit(6).exec(function(err, docs) {
				productContent3.push(docs);
			});

		var productMain1 = [];
		var productMain2 = [];
		var productMain3 = [];
		var productMain4 = [];
		// productMain1 3..... từ vị trí 25 lấy 5 phần tử
			Product.find().skip(24).limit(5).exec(function(err, docs) {
				productMain1.push(docs);
			});
		// productMain1 3..... từ vị trí 30 lấy 5 phần tử
			Product.find().skip(29).limit(5).exec(function(err, docs) {
				productMain2.push(docs);
			});
		// productMain1 3..... từ vị trí 35 lấy 5 phần tử
			Product.find().skip(34).limit(5).exec(function(err, docs) {
				productMain3.push(docs);
			});
		// productMain1 3..... từ vị trí 40 lấy 5 phần tử
			Product.find().skip(39).limit(5).exec(function(err, docs) {
				productMain4.push(docs);
			});
		
			

		var RecentlyViewed = [];
		// productContent 3.....16 -> 22
			Product.find().skip(0).limit(10).exec(function(err, docs) {
				RecentlyViewed.push(docs);
			});
		
		// tra ve du lieu
		res.render('shop/home', {
			title: 'shopping cart',
			successMsg: successMsg, 
			noMessages: !successMsg,
			productsItems: productsItems,
			user: user,
			cartItems: cart.generateArray(),
			products: productChunks,
			products1: productChunks1,
			products2: productChunks2,
			productContent: productContent,
			productContent1: productContent1,
			productContent2: productContent2,
			productContent3: productContent3,
			productMain1: productMain1,
			productMain2: productMain2,
			productMain3: productMain3,
			productMain4: productMain4,
			RecentlyViewed: RecentlyViewed
		});
	});
};

