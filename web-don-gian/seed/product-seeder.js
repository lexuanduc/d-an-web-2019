/*const Product = require('../models/product');
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/shopping');

var products = [ 
	new Product({
		name: "sp moi",
		images: "https://loremflickr.com/320/240",
		price: 1000,
		description: "nunc rhoncus dui vel sem sed sagittis nam"
	}),
	new Product({
		name: "sp cu",
		images: "https://loremflickr.com/320/240",
		price: 2000,
		description: "nunc rhoncus dui vel sem sed sagittis nam"
	}),
	new Product({
		name: "sp sasas",
		images: "https://loremflickr.com/320/240",
		price: 3000,
		description: "nunc rhoncus dui vel sem sed sagittis nam"
	}),
	new Product({
		name: "sp ton kho",
		images: "https://loremflickr.com/320/240",
		price: 4000,
		description: "nunc rhoncus dui vel sem sed sagittis nam"
	})
];

var done = 0;
for( var i = 0; i < products.length; i++) {
	products[i].save(function(err, result) {
		done++;
		if (done === products.length) {
			exit();
		}
	});
}

function exit() {
	mongoose.disconnect();
}
*/

var ProductsHome = require('../models/product-home');
var mongoose = require('mongoose');
var mongoDB = 'mongodb://localhost:27017/shopping';
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
mongoose.connect(mongoDB, function(err, db) {
    if (err) {
        console.log('MongoDB connection fail');
        throw err;
    } else {
        console.log('MongoDB connection successful');
    }
});

var productHome = [
  new ProductsHome ({
    name: "abc",
    title : "Su34",
    images : "http://media2.netnews.vn/archive/images/2016/01/07/171109_3.jpg",
    price : 1000,
    salePrice: 200
  }),
  new ProductsHome ({
    name: "abc",
    title : "Su34",
    images : "http://media2.netnews.vn/archive/images/2016/01/07/171109_3.jpg",
    price : 2000,
    salePrice: 100
  }),
  new ProductsHome ({
    name: "abc",
    title : "Su34",
    images : "http://media2.netnews.vn/archive/images/2016/01/07/171109_3.jpg",
    price : 3000,
    salePrice: 300
  })

];

var done = 0;
for (var i = 0; i < productHome.length; i++){
    productHome[i].save(function (err, result) {
        done++;
        console.log(done);
        if (done === productHome.length) {
            exit();
        }
    });
};

function exit(){
    mongoose.disconnect();
}
